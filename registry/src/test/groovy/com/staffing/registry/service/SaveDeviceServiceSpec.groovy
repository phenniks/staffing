package com.staffing.registry.service

import com.staffing.registry.exception.ConfigNotFoundException
import com.staffing.registry.integration.ConfigurationServiceFacade
import com.staffing.registry.integration.model.Config
import com.staffing.registry.model.Device
import com.staffing.registry.model.NewDevice
import com.staffing.registry.repo.DeviceRepository
import com.staffing.registry.service.mapper.DeviceMapper
import reactor.core.publisher.Mono
import spock.lang.Specification

class SaveDeviceServiceSpec extends Specification {

    DeviceRepository deviceRepository = Mock()
    ConfigurationServiceFacade configurationServiceFacade = Mock()
    DeviceMapper mapper = Mock()

    def target = new SaveDeviceService(deviceRepository, configurationServiceFacade, mapper)

    def serNum = "0120ASF00DS"
    def ip = "192.168.0.1"
    def netmask = "255.255.255.0"
    def newDevice = new NewDevice(serNum, "Apple", "iMac", "qw-qw-we-ew-er")
    def config = new Config(serNum, ip, netmask)
    def device = Device.builder()
            .serNum(serNum)
            .vendor(newDevice.getVendor())
            .model(newDevice.getModel())
            .mac(newDevice.getMac())
            .ip(ip)
            .netmask(netmask)
            .build()

    def "should fetch config and save device to repository" () {
        when:
        def result = target.save(newDevice).block()
        then:
        result == device
        1 * deviceRepository.save(device) >> Mono.just(device)
        1 * configurationServiceFacade.getConfig(serNum) >> Mono.just(config)
        1 * mapper.map(newDevice, config) >> device
        0 * _
    }

    def "should return error if config return error" () {
        when:
        target.save(newDevice).block()
        then:
        thrown(ConfigNotFoundException)
        1 * configurationServiceFacade.getConfig(serNum) >> Mono.error(new ConfigNotFoundException())
        0 * _
    }
}
