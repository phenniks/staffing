package com.staffing.registry.api

import com.staffing.registry.BaseRegistrySpecification
import org.hamcrest.Matchers

import static com.jayway.restassured.RestAssured.when
import static org.apache.http.HttpStatus.SC_OK

class SwaggerSpec extends BaseRegistrySpecification {

    def "should expose Swagger Ui"() {
        expect:
        when().get("/swagger-ui.html")
                .then()
                .statusCode(SC_OK)
    }

    def "should expose swagger API for group"() {
        expect:
        when().get("/v2/api-docs")
                .then()
                .statusCode(SC_OK)
                .body("info.title", Matchers.is("Registry service"))
    }
}
