package com.staffing.registry.api

import com.staffing.registry.BaseRegistrySpecification
import com.staffing.registry.model.Device
import com.staffing.registry.model.NewDevice
import spock.lang.Stepwise

import static com.staffing.registry.testutils.stub.StubApi.stubConfigService

@Stepwise
class ConfigApiSpec extends BaseRegistrySpecification {

    def serNum = "0120ASF00DS"
    def ip = "192.168.0.1"
    def netmask = "255.255.255.0"
    def newDevice = new NewDevice(serNum, "Apple", "iMac", "qw-qw-we-ew-er")
    def expected = Device.builder()
            .serNum(serNum)
            .vendor(newDevice.getVendor())
            .model(newDevice.getModel())
            .mac(newDevice.getMac())
            .ip(ip)
            .netmask(netmask)
            .build()

    def "should save device"() {
        given:
        stubConfigService(serNum, ip, netmask)
        when:
        def saveResponse = registryApi.saveDevice(newDevice)
        Thread.sleep(50000)
        then:
        saveResponse.body().as(Device) == expected
    }

    def "should fetch device by serial number"() {
        expect:
        registryApi.getDeviceBySerialNumber(serNum).body().as(Device) == expected
    }

    def "should fetch all devices"() {
        expect:
        registryApi.getDevices().body().as(Device[]) == [expected]
    }

    def "should fetch devices by vendor"() {
        expect:
        registryApi.getDevicesByVendor(newDevice.vendor).body().as(Device[]) == [expected]
        and:
        registryApi.getDevicesByVendor("incorrect vendor").body().as(Device[]) == []
    }

    def "should fetch devices by model"() {
        expect:
        registryApi.getDevicesByModel(newDevice.model).body().as(Device[]) == [expected]
        and:
        registryApi.getDevicesByVendor("incorrect model").body().as(Device[]) == []
    }

    def "should delete device by serial number"() {
        when:
        registryApi.deleteDevice(serNum)
        then:
        registryApi.getDeviceBySerialNumber(serNum).then().statusCode(500)
    }
}
