package com.staffing.registry

import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.jayway.restassured.RestAssured
import com.staffing.registry.testutils.RegistryApiClient
import org.junit.Rule
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = [RegistryApplication, TestContext])
@ActiveProfiles("test")
class BaseRegistrySpecification extends Specification {

    @LocalServerPort
    protected int serverPort

    @Rule
    WireMockRule wireMockRule = new WireMockRule(8080)

    def setup() {
        RestAssured.port = serverPort
        WireMock.reset()
    }

    @Autowired
    RegistryApiClient registryApi

    @Configuration
    static class TestContext {

        @Bean
        RegistryApiClient registryApi() {
            new RegistryApiClient()
        }
    }

}
