package com.staffing.registry.testutils.stub

import static com.github.tomakehurst.wiremock.client.WireMock.*
import static com.staffing.registry.testutils.stub.TestData.template

class StubApi {

    static stubConfigService(def serNum, def ip, def netmask) {
        stubFor(get(urlEqualTo("/config/${serNum}"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(template("config-service-response.json")
                                .make([serNum : serNum,
                                       ip     : ip,
                                       netmask: netmask])
                                .toString())
                )
        )
    }
}
