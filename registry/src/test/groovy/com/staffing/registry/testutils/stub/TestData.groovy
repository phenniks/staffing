package com.staffing.registry.testutils.stub

import groovy.text.SimpleTemplateEngine
import groovy.text.Template

class TestData {

    static Template template(String fileName) {
        def response = TestData.class.getClassLoader().getResourceAsStream(fileName).text
        new SimpleTemplateEngine().createTemplate(response)
    }
}
