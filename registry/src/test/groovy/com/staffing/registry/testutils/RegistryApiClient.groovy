package com.staffing.registry.testutils

import com.jayway.restassured.response.Response
import com.staffing.registry.model.NewDevice

import static com.jayway.restassured.RestAssured.given
import static com.jayway.restassured.RestAssured.when

class RegistryApiClient {

    Response saveDevice(NewDevice newDevice) {
        given()
                .header("Content-Type", "application/json")
                .body(newDevice)
                .when()
                .post("/device")
    }

    Response getDeviceBySerialNumber(String serNum) {
        when().get("/device/$serNum")
    }

    Response getDevicesByVendor(String vendor) {
        when().get("/device/vendor/$vendor")
    }

    Response getDevicesByModel(String model) {
        when().get("/device/model/$model")
    }

    Response getDevices() {
        when().get("/devices")
    }

    Response deleteDevice(String serNum) {
        when().delete("/device/$serNum")
    }
}
