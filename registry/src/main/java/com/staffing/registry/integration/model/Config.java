package com.staffing.registry.integration.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
@JsonDeserialize(builder = Config.ConfigBuilder.class)
public class Config {

    @NonNull
    private final String serNum;

    @NonNull
    private final String ip;

    @NonNull
    private final String netmask;

    @JsonPOJOBuilder(withPrefix = "")
    public static class ConfigBuilder {
    }
}

