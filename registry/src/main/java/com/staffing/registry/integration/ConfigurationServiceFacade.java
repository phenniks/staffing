package com.staffing.registry.integration;

import com.staffing.registry.exception.ConfigNotFoundException;
import com.staffing.registry.integration.model.Config;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Slf4j
@AllArgsConstructor
public class ConfigurationServiceFacade {

    private WebClient.Builder webClientBuilder;
    private String configServiceUrl;

    public Mono<Config> getConfig(String serialNumber) {
        return webClientBuilder
                .baseUrl(configServiceUrl)
                .build()
                .get()
                .uri("/config/" + serialNumber)
                .retrieve()
                .bodyToMono(Config.class)
                .doOnSuccess(config -> log.info("Fetched config from config service: {}", config))
                .switchIfEmpty(Mono.error(() -> new ConfigNotFoundException(serialNumber)));
    }
}
