package com.staffing.registry.service.mapper;

import com.staffing.registry.integration.model.Config;
import com.staffing.registry.model.NewDevice;
import com.staffing.registry.model.Device;

public class DeviceMapper {

    public Device map(NewDevice newDevice, Config config) {
        return Device.builder()
                .serNum(newDevice.getSerNum())
                .vendor(newDevice.getVendor())
                .model(newDevice.getModel())
                .mac(newDevice.getMac())
                .ip(config.getIp())
                .netmask(config.getNetmask())
                .build();
    }
}
