package com.staffing.registry.service;

import com.staffing.registry.integration.ConfigurationServiceFacade;
import com.staffing.registry.model.NewDevice;
import com.staffing.registry.model.Device;
import com.staffing.registry.repo.DeviceRepository;
import com.staffing.registry.service.mapper.DeviceMapper;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Mono;

@AllArgsConstructor
public class SaveDeviceService {

    private DeviceRepository deviceRepository;
    private ConfigurationServiceFacade configurationServiceFacade;
    private DeviceMapper mapper;

    public Mono<Device> save(NewDevice newDevice) {
        return configurationServiceFacade.getConfig(newDevice.getSerNum())
                .map(config -> mapper.map(newDevice, config))
                .flatMap(registry -> deviceRepository.save(registry));
    }
}
