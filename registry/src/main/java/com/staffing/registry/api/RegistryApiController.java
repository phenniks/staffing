package com.staffing.registry.api;

import com.staffing.registry.exception.DeviceNotFoundException;
import com.staffing.registry.model.NewDevice;
import com.staffing.registry.model.Device;
import com.staffing.registry.repo.DeviceRepository;
import com.staffing.registry.service.SaveDeviceService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@Slf4j
@RestController
@AllArgsConstructor
public class RegistryApiController {

    private DeviceRepository deviceRepository;
    private SaveDeviceService saveDeviceService;

    @ApiOperation(value = "Endpoint to save device by serial number")
    @PostMapping(path = "/device", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public Mono<Device> saveDevice(@RequestBody NewDevice newDevice) {
        return saveDeviceService.save(newDevice)
                .doOnSuccess(device -> log.info("Device {} was saved", device));
    }

    @ApiOperation(value = "Endpoint to fetch device by serial number")
    @GetMapping(path = "/device/{serialNumber}", produces = APPLICATION_JSON_UTF8_VALUE)
    public Mono<Device> getDeviceBySerialNumber(@PathVariable String serialNumber) {
        return deviceRepository.findBySerNum(serialNumber)
                .doOnSuccess(config -> log.info("Fetch configuration: {}", config))
                .switchIfEmpty(Mono.error(() -> new DeviceNotFoundException(serialNumber)));
    }

    @ApiOperation(value = "Endpoint to fetch devices by vendor")
    @GetMapping(path = "/device/vendor/{vendor}", produces = APPLICATION_JSON_UTF8_VALUE)
    public Flux<Device> getDevicesByVendor(@PathVariable String vendor) {
        return deviceRepository.findAllByVendor(vendor);
    }

    @ApiOperation(value = "Endpoint to fetch devices by model")
    @GetMapping(path = "/device/model/{model}", produces = APPLICATION_JSON_UTF8_VALUE)
    public Flux<Device> getDevicesByModel(@PathVariable String model) {
        return deviceRepository.findAllByModel(model);
    }

    @ApiOperation(value = "Endpoint to fetch all devices")
    @GetMapping(path = "/devices", produces = APPLICATION_JSON_UTF8_VALUE)
    public Flux<Device> getDevices() {
        return deviceRepository.findAll();
    }

    @ApiOperation(value = "Endpoint to delete device by serial number")
    @DeleteMapping(path = "/device/{serialNumber}")
    public Mono<Void> deleteDevice(@PathVariable String serialNumber) {
        return deviceRepository.deleteById(serialNumber)
                .doOnSuccess(config -> log.info("Device with id {} was removed", serialNumber));
    }
}
