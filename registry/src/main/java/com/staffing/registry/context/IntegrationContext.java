package com.staffing.registry.context;

import com.staffing.registry.integration.ConfigurationServiceFacade;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class IntegrationContext {

    @Value("${service.configuration.url}")
    private String configServiceUrl;

    @Bean
    @LoadBalanced
    public WebClient.Builder webClientBuilder() {
        return WebClient.builder();
    }

    @Bean
    public ConfigurationServiceFacade configurationServiceFacade(WebClient.Builder webClientBuilder) {
        return new ConfigurationServiceFacade(webClientBuilder, configServiceUrl);
    }
}
