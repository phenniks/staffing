package com.staffing.registry.context;

import com.staffing.registry.api.RegistryApiController;
import com.staffing.registry.integration.ConfigurationServiceFacade;
import com.staffing.registry.repo.DeviceRepository;
import com.staffing.registry.service.SaveDeviceService;
import com.staffing.registry.service.mapper.DeviceMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppContext {

    @Bean
    public RegistryApiController registryApiController(DeviceRepository deviceRepository,
                                                       SaveDeviceService saveDeviceService) {
        return new RegistryApiController(deviceRepository, saveDeviceService);
    }

    @Bean
    public SaveDeviceService saveDeviceService(DeviceRepository deviceRepository,
                                               ConfigurationServiceFacade configurationServiceFacade,
                                               DeviceMapper deviceMapper) {
        return new SaveDeviceService(deviceRepository, configurationServiceFacade, deviceMapper);
    }

    @Bean
    public DeviceMapper deviceMapper() {
        return new DeviceMapper();
    }
}
