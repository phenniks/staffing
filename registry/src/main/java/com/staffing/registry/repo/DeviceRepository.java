package com.staffing.registry.repo;

import com.staffing.registry.model.Device;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DeviceRepository extends ReactiveMongoRepository<Device, String> {

    Mono<Device> findBySerNum(String serNum);

    Flux<Device> findAllByModel(String model);

    Flux<Device> findAllByVendor(String model);
}
