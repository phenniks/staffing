package com.staffing.registry.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document
@Builder
public class Device {

    @Id
    @NonNull
    private String serNum;

    @Indexed
    @NonNull
    private String vendor;

    @Indexed
    @NonNull
    private String model;

    @NonNull
    private String mac;

    @NonNull
    private String ip;

    @NonNull
    private String netmask;
}
