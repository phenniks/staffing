package com.staffing.registry.model;

import lombok.Data;
import lombok.NonNull;

@Data
public class NewDevice {

    @NonNull
    private final String serNum;

    @NonNull
    private final String vendor;

    @NonNull
    private final String model;

    @NonNull
    private final String mac;
}
