package com.staffing.registry.exception;

public class DeviceNotFoundException extends RuntimeException {
    public DeviceNotFoundException(String serNum) {
        super(String.format("Device with serial number %s not found", serNum));
    }
}
