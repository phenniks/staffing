package com.staffing.registry.exception;

public class ConfigNotFoundException extends RuntimeException {
    public ConfigNotFoundException(String serNum) {
        super(String.format("Config with serial number %s not found", serNum));
    }
}
