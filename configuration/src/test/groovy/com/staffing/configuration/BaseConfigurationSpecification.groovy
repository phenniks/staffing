package com.staffing.configuration

import com.jayway.restassured.RestAssured
import com.staffing.configuration.testutils.ConfigApiClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = [ConfigurationApplication, TestContext])
@ActiveProfiles("test")
class BaseConfigurationSpecification extends Specification {

    @LocalServerPort
    protected int serverPort

    def setup() {
        RestAssured.port = serverPort
    }

    @Autowired
    ConfigApiClient configApi

    @Configuration
    static class TestContext {

        @Bean
        ConfigApiClient configApi() {
            new ConfigApiClient()
        }
    }

}
