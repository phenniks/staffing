package com.staffing.configuration.model

import com.google.common.testing.NullPointerTester
import spock.lang.Specification

class NullChecksRepoModelsSpec extends Specification {

    def nullPointerTester = new NullPointerTester()

    def "should verify null checks config model"() {
        expect:
        nullPointerTester.testConstructors(Config, NullPointerTester.Visibility.PACKAGE)
    }
}
