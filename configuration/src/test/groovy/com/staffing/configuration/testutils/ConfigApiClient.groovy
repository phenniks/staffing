package com.staffing.configuration.testutils

import com.jayway.restassured.response.Response
import com.staffing.configuration.model.Config

import static com.jayway.restassured.RestAssured.given
import static com.jayway.restassured.RestAssured.when

class ConfigApiClient {

    Response saveConfig(Config config) {
        given()
                .header("Content-Type", "application/json")
                .body(config)
                .when()
                .post("/config")
    }

    Response getConfig(String serNum) {
        when().get("/config/$serNum")
    }

    Response getConfigs() {
        when().get("/configs")
    }

    Response deleteConfig(String serNum) {
        when().delete("/config/$serNum")
    }
}
