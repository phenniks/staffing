package com.staffing.configuration.api

import com.staffing.configuration.BaseConfigurationSpecification
import com.staffing.configuration.model.Config

class ConfigApiSpec extends BaseConfigurationSpecification {

    def "should save and return configuration" () {
        given:
        def serNum = "serNum"
        def config = new Config(serNum, "ip", "netmask")

        when: "save config"
        def saveResponse = configApi.saveConfig(config)
        Thread.sleep(50000)
        then: "validate config"
        saveResponse.body().as(Config) == config
        and:
        configApi.getConfig(serNum).body().as(Config) == config
        and:
        configApi.getConfigs().body().as(Config[]) == [config]

        when:
        configApi.deleteConfig(serNum)
        then:
        configApi.getConfig(serNum).body().asString().isEmpty()
    }
}
