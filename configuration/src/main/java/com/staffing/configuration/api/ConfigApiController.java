package com.staffing.configuration.api;

import com.staffing.configuration.model.Config;
import com.staffing.configuration.repo.ConfigRepository;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@Slf4j
@RestController
@AllArgsConstructor
public class ConfigApiController {

    private ConfigRepository configRepository;

    @ApiOperation(value = "Endpoint to save config by serial number")
    @PostMapping(path = "/config", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public Mono<Config> saveConfig(@RequestBody Config config) {
        return configRepository.save(config)
                .doOnSuccess(cnf -> log.info("Save configuration: {}", cnf));
    }

    @ApiOperation(value = "Endpoint to fetch config by serial number")
    @GetMapping(path = "/config/{serialNumber}", produces = APPLICATION_JSON_UTF8_VALUE)
    public Mono<Config> getConfig(@PathVariable String serialNumber) {
        return configRepository.findBySerNum(serialNumber)
                .doOnSuccess(config -> log.info("Fetch configuration: {}", config));
    }

    @ApiOperation(value = "Endpoint to fetch list of all configs")
    @GetMapping(path = "/configs", produces = APPLICATION_JSON_UTF8_VALUE)
    public Flux<Config> getConfigs() {
        return configRepository.findAll();
    }

    @ApiOperation(value = "Endpoint to delete config by serial number")
    @DeleteMapping(path = "/config/{serialNumber}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Mono<Void> deleteConfig(@PathVariable String serialNumber) {
        return configRepository.deleteById(serialNumber)
                .doOnSuccess(config -> log.info("Config with id {} was removed", serialNumber));
    }
}
