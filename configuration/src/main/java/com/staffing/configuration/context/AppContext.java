package com.staffing.configuration.context;

import com.staffing.configuration.api.ConfigApiController;
import com.staffing.configuration.repo.ConfigRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppContext {

    @Bean
    public ConfigApiController configApiController(ConfigRepository configRepository) {
        return new ConfigApiController(configRepository);
    }
}
