package com.staffing.configuration.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Config {

    @Id
    @NonNull
    private String serNum;

    @NonNull
    private String ip;

    @NonNull
    private String netmask;
}
