package com.staffing.configuration.repo;

import com.staffing.configuration.model.Config;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface ConfigRepository extends ReactiveMongoRepository<Config, String> {

    Mono<Config> findBySerNum(String serNum);
}
